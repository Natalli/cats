import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { CONSTANTS } from '../constants';
import { ICat } from '../interfaces/cat';

@Injectable({
  providedIn: 'root'
})
export class CatsService {
  endpoint;
  headers;

  constructor(
    private http: HttpClient
  ) {
    this.endpoint = 'https://api.thecatapi.com/v1/';
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': CONSTANTS.API_CODE
    });
  }

  getCats() {
    return this.http.get<Array<ICat>>(this.endpoint + 'images/search?limit=10', { headers: this.headers })
  }

  getBreeds() {
    return this.http.get<any>(this.endpoint + `breeds`, { headers: this.headers });
  }

  getCatsByParams(id: string) {
    let params = new HttpParams()
    const dataParams = {
      breed_ids: id,
      limit: 10
    };
    for (const [key, value] of Object.entries(dataParams)) {
      params = params.append(`${key}`, `${value}`);
    }
    return this.http.get<any>(this.endpoint + 'images/search', { params, headers: this.headers })
  }

}
