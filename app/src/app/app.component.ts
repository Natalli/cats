import { Component } from '@angular/core';
import { CatsService } from './services/cats.service';
import { ICat } from './interfaces/cat';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Cats';
  cats: Array<ICat>;

  constructor(
    private catService: CatsService
  ) {
    this.cats = [];
  }

  ngOnInit() {
    this.getAllCats();
  }

  getAllCats() {
    this.catService.getCats().subscribe(resp => {
      this.cats = resp;
    })
  }

  getCatsByBreed(data: string) {
    this.catService.getCatsByParams(data).subscribe(resp => {
      this.cats = resp;
    })
  }

  clear() {
    this.cats = [];
    this.getAllCats();
  }

}

