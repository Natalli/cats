import { Component, Input, OnInit } from '@angular/core';
import { ICat } from 'src/app/interfaces/cat';

@Component({
  selector: 'app-cats-list',
  templateUrl: './cats-list.component.html',
  styleUrls: ['./cats-list.component.scss']
})
export class CatsListComponent implements OnInit {
  cats: Array<ICat>;
  @Input() set data(value: any) {
    if (value) {
      this.cats = value;
    }
  }
  constructor() {
    this.cats = [];
  }

  ngOnInit(): void {
  }

}
