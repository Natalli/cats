import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICat } from 'src/app/interfaces/cat';
import { CatsService } from 'src/app/services/cats.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  @Output() onSelected: EventEmitter<any>  = new EventEmitter();
  @Output() onClear: EventEmitter<any>  = new EventEmitter();

  form: FormGroup = new FormGroup({});
  selectedBreed: string = '';
  cats: Array<ICat>;
  breeds: Array<any>;
  constructor(
    private catService: CatsService,
    private formBuilder: FormBuilder
  ) {
    this.cats = [];
    this.breeds = [];
  }

  ngOnInit(): void {
    this.createForm();
    this.getBreeds();
  }


  createForm() {
    this.form = this.formBuilder.group({
      breed_ids: ['', Validators.required]
    });
  }


  getBreeds() {
    this.catService.getBreeds().subscribe(resp => {
      this.breeds = resp;
    })
  }

  getCatsByBreed() {
    if (this.form.value.breed_ids) {
      this.selectedBreed = this.form.value.breed_ids;
      this.onSelected.emit(this.selectedBreed);
    }
  }

  clear() {
    this.selectedBreed = '';
    this.form.get('breed_ids')?.reset();
    this.onClear.emit();
  }

}
