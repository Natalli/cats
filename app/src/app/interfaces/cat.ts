export interface ICat {
  height: number,
  width: number,
  id: string,
  url: string,
  breeds: any[]
}
